import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Brand, TransmissionType, BodyType } from '../constants';
import { CarPhoto } from './car-photo.entity';

@Entity('car')
export class Car {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'registration_number',
    type: 'varchar',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  registrationNumber: string;

  @Column({
    name: 'brand',
    type: 'enum',
    enum: Brand,
    collation: 'utf8_general_ci',
  })
  brand: Brand;

  @Column({
    name: 'model',
    type: 'varchar',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  model: string;

  @Column({
    name: 'body_type',
    type: 'enum',
    enum: BodyType,
    nullable: false,
    collation: 'utf8_general_ci',
  })
  bodyType: BodyType;

  @Column({
    name: 'variant',
    type: 'varchar',
    nullable: true,
    collation: 'utf8_general_ci',
  })
  variant: string;

  @Column({
    name: 'registered_year',
    type: 'integer',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  registeredYear: number;

  @Column({
    name: 'price',
    type: 'double',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  price: number;

  @Column({
    name: 'sell_price',
    type: 'double',
    nullable: false,
  })
  sellPrice: number;

  @Column({
    name: 'interest',
    type: 'float',
    nullable: true,
  })
  interest: number;

  @Column({
    name: 'mileage',
    type: 'integer',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  mileage: number;

  @Column({
    name: 'transmission',
    type: 'enum',
    enum: TransmissionType,
    nullable: false,
    collation: 'utf8_general_ci',
  })
  transmission: TransmissionType;

  @Column({
    name: 'color',
    type: 'varchar',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  color: string;

  @Column({
    name: 'status',
    type: 'varchar',
    collation: 'utf8_general_ci',
    default: 'available',
  })
  status: string;

  @Column({
    name: 'seat_capacity',
    type: 'integer',
    nullable: false,
  })
  seatCapacity: number;

  @Column({
    name: 'engine_capacity',
    type: 'integer',
    nullable: false,
  })
  engineCapacity: number;

  @Column({
    name: 'remark',
    type: 'varchar',
    nullable: true,
    collation: 'utf8_general_ci',
  })
  remark: string;

  @OneToMany(type => CarPhoto, carPhoto => carPhoto.car)
  carPhotos: CarPhoto[];
}
