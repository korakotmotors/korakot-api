import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Roles } from '../constants';

@Entity('users')
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'email',
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    name: 'password',
    type: 'varchar',
    nullable: false,
  })
  password: string;

  @Column({
    name: 'name',
    type: 'varchar',
    nullable: true,
  })
  name: string;

  @Column({
    name: 'role',
    type: 'enum',
    enum: Roles,
    default: Roles.STAFF,
  })
  role: Roles;
}
