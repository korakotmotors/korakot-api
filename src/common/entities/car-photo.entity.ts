import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Car } from './car.entity';

@Entity('car_photo')
export class CarPhoto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'photo_name',
    type: 'varchar',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  photoName: string;

  @Column({
    name: 'photo_path',
    type: 'varchar',
    nullable: false,
    collation: 'utf8_general_ci',
  })
  photoPath: string;

  @Column({
    name: 'photo_url',
    type: 'varchar',
    nullable: true,
    collation: 'utf8_general_ci',
  })
  photoUrl: string;

  @ManyToOne(type => Car, car => car.carPhotos)
  car: Car;
}
