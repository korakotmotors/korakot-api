export interface UploadedData extends Express.Multer.File {
  publicUrl: string;
}
