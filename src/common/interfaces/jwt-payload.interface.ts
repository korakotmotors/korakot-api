export interface JwtPayload {
  id: number;
  hash: string;
  email: string;
}
