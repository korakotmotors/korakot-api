import { DeepPartial } from 'typeorm';

export interface BaseService<T> {
  findAll(): Promise<T[]>;
  create(dto: any): T;
}
