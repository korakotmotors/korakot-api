import { ConfigService } from '../modules/config/config.service';

export enum Brand {
  TOYOTA = 'Toyota',
  HONDA = 'Honda',
  MITSUBISHI = 'Mitsubishi',
  IZUSU = 'Izusu',
  MAZDA = 'Mazda',
  NISSAN = 'Nissan',
  BMW = 'BMW',
  BENZ = 'Benz',
  CHEVROLET = 'Chevrolet',
  FORD = 'Ford',
  SUZUKI = 'Suzuki',
  LAND_ROVER = 'Land Rover',
  DFM = 'DFM',
  VOLKSWAGEN = 'Volkswagen',
  VOLVO = 'Volvo',
}

export enum TransmissionType {
  AUTOMATIC = 'Automatic',
  MANUAL = 'Manual',
}

export enum BodyType {
  SEDAN = 'Sedan',
  PICKUP = 'Pickup',
  SUV = 'SUV',
  HATCHBACK = 'Hatchback',
}

export enum Roles {
  OWNER = 'Owner',
  EDITOR = 'Editor',
  MANAGER = 'Manager',
  STAFF = 'Staff',
}

export function getTokenKey(id: number): string {
  return `SESSION:${id}`;
}
