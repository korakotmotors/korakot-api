import { Injectable } from '@nestjs/common';
import { promisify } from 'util';
import { ConfigService } from './config/config.service';
import * as Redis from 'ioredis';

@Injectable()
export class RedisService {
  constructor(private readonly configService: ConfigService) {}
  private client = new Redis(this.configService.getRedisConnectionString());
  private setAsync = promisify(this.client.set).bind(this.client);
  private getAsync = promisify(this.client.get).bind(this.client);
  private mgetAsync = promisify(this.client.mget).bind(this.client);
  private delAsync = promisify(this.client.del).bind(this.client);

  async get(key: string) {
    return await this.getAsync(key);
  }

  async set(key: string, value: string, duration: number) {
    return await this.setAsync(key, value, 'EX', duration);
  }

  async mget(key: string[]) {
    return await this.mgetAsync([...key]);
  }

  async deleteToken(key: string) {
    return await this.delAsync(key);
  }
}
