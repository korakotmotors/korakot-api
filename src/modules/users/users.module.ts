import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from '../../common/entities/users.entity';
import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { BcryptService } from './bcrypt.service';
import { UsersController } from './users.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Users])],
  controllers: [UsersController],
  providers: [UsersService, BcryptService],
  exports: [UsersService],
})
export class UsersModule {}
