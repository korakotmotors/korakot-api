import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { hashSync, genSaltSync, compareSync } from 'bcryptjs';

@Injectable()
export class BcryptService {
  constructor(private readonly configService: ConfigService) {}
  hashPassword(plainPassword: string): string {
    const salt = genSaltSync(
      Number(this.configService.getString('PASSWORD_SALT')),
    );
    return hashSync(plainPassword, salt);
  }

  verifyPassword(plainPassword: string, hashedPassword: string): boolean {
    return compareSync(plainPassword, hashedPassword);
  }
}
