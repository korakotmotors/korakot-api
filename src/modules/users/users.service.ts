import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Users } from '../../common/entities/users.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { BcryptService } from './bcrypt.service';
import { CreateUserDTO } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private readonly usersRepository: Repository<Users>,
    private readonly bcrypt: BcryptService,
  ) {}

  async findOneByEmail(email: string): Promise<Users> {
    return this.usersRepository.findOne({ email });
  }

  async auth(email: string): Promise<Users> {
    return await this.usersRepository.findOne(
      {
        email,
      },
      { select: ['id', 'email', 'password'] },
    );
  }

  verifyPassword(plainPassword: string, hasedPassword: string): boolean {
    return this.bcrypt.verifyPassword(plainPassword, hasedPassword);
  }

  async createUser(createUserDto: CreateUserDTO): Promise<Users> {
    const hashedPassword = this.bcrypt.hashPassword(createUserDto.password);
    const newUser = { ...createUserDto, password: hashedPassword };
    const createdUser = this.usersRepository.create(newUser);
    return await this.usersRepository.save(createdUser);
  }
}
