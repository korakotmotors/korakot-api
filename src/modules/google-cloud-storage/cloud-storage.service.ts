import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import {
  Storage,
  Bucket,
  GetBucketResponse,
  File,
} from '@google-cloud/storage';
import { ConfigService } from '../../../dist/modules/config/config.service';
import { UploadedData } from '../../common/interfaces/uploaded-data.interface';
import { createReadStream } from 'fs';

@Injectable()
export class CloudStorageService {
  private readonly storage: Storage;
  constructor(private readonly configService: ConfigService) {
    this.storage = new Storage({
      projectId: this.configService.getString('GCS_PROJECT_ID'),
      keyFilename: this.configService.getString('GCS_KEY_FILENAME'),
    });
  }

  private async getBucket(bucketName: string): Promise<Bucket> {
    return await this.storage.bucket(bucketName);
  }

  async uploadFile(
    bucketName: string,
    file: Express.Multer.File,
  ): Promise<UploadedData> {
    const bucket = await this.getBucket(bucketName);
    return new Promise(async (resolve, reject) => {
      const { filename, originalname, path, mimetype } = file;
      const [uploadedFile] = await bucket.upload(path, {
        metadata: {
          contentType: mimetype,
        },
      });
      await uploadedFile.makePublic();
      const newFile = {
        ...file,
        publicUrl: `https://storage.googleapis.com/${bucketName}/${filename}`,
      };
      resolve(newFile);
    });
  }
}
