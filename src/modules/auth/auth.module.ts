import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '../config/config.service';
import { RedisService } from '../redis.service';
import { UsersService } from '../users/users.service';
import { BcryptService } from '../users/bcrypt.service';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (configService: ConfigService) => ({
        secret: configService.getString('JWT_SECRET'),
        signOptions: { expiresIn: '7days' },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [AuthService, RedisService, BcryptService],
  exports: [AuthService],
})
export class AuthModule {}
