import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '../../../dist/modules/config/config.service';
import { AuthService } from './auth.service';
import { JwtPayload } from '../../common/interfaces/jwt-payload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private readonly configService: ConfigService,
    private readonly authService: AuthService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.getString('JWT_SECRET'),
    });
  }

  async validate(payload: JwtPayload) {
    const authPayload = await this.authService.validateJwt(payload);
    if (!authPayload) {
      throw new UnauthorizedException(
        'Wrong email/password or your session maybe expired.',
      );
    }
    return authPayload;
  }
}
