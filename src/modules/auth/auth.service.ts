import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { getTokenKey } from '../../common/constants';
import { JwtPayload } from '../../common/interfaces/jwt-payload.interface';
import { JwtService } from '@nestjs/jwt';
import { LoginDTO } from './dto/login.dto';
import { BcryptService } from '../users/bcrypt.service';
import { RedisService } from '../redis.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly bcrypt: BcryptService,
    private readonly redisService: RedisService,
  ) {}

  createToken(payload: JwtPayload): string {
    return this.jwtService.sign(payload);
  }

  async login(loginDto: LoginDTO) {
    const user = await this.usersService.auth(loginDto.email);
    if (
      !user ||
      !this.usersService.verifyPassword(loginDto.password, user.password)
    ) {
      throw new UnauthorizedException('Wrong email or password');
    }

    const hash = this.bcrypt.hashPassword(`${Date.now()}`);
    const token = this.createToken({
      hash,
      id: user.id,
      email: user.email,
    });
    const key = getTokenKey(user.id);
    await this.redisService.set(key, hash, 604800);
    return { token };
  }

  async validateJwt(payload: JwtPayload): Promise<JwtPayload> {
    const { id, hash } = payload;
    const key = getTokenKey(id);
    const result = await this.redisService.get(key);
    if (result === hash) {
      return payload;
    }
    throw new UnauthorizedException('Invalid session or session has expired');
  }
}
