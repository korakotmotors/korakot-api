import { Repository, EntityRepository } from 'typeorm';
import { Car } from '../../common/entities/car.entity';

@EntityRepository(Car)
export class CarRepository extends Repository<Car> {
  async findAvailable(): Promise<Car[]> {
    return this.find({
      where: { status: 'available' },
    });
  }
}
