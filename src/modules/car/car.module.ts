import { Module } from '@nestjs/common';
import { CarController } from './car.controller';
import { CarService } from './car.service';
import { CarRepository } from './car.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from '../../common/entities/car.entity';
import { CarPhoto } from '../../common/entities/car-photo.entity';
import { CloudStorageModule } from '../google-cloud-storage/cloud-storage.module';
import { CloudStorageService } from '../google-cloud-storage/cloud-storage.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Car, CarRepository, CarPhoto]),
    CloudStorageModule,
  ],
  controllers: [CarController],
  providers: [CarService, CloudStorageService],
})
export class CarModule {}
