import { Injectable } from '@nestjs/common';
import { CarRepository } from './car.repository';
import { Car } from '../../common/entities/car.entity';
import { InsertCarDto } from './dto/insert-car.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateCarDto } from './dto/update-car.dto';
import { CarPhoto } from '../../common/entities/car-photo.entity';
import { Repository } from 'typeorm';
import { CloudStorageService } from '../google-cloud-storage/cloud-storage.service';
import { UploadedData } from '../../common/interfaces/uploaded-data.interface';

@Injectable()
export class CarService {
  constructor(
    @InjectRepository(CarRepository)
    private readonly carRepository: CarRepository,
    @InjectRepository(CarPhoto)
    private readonly carPhotoRepository: Repository<CarPhoto>,
    private readonly cloudStorageService: CloudStorageService,
  ) {}

  async findAll(): Promise<Car[]> {
    return await this.carRepository.findAvailable();
  }

  create(createCarDto: InsertCarDto): Car {
    return this.carRepository.create(createCarDto);
  }

  async findById(id: number): Promise<Car> {
    return await this.carRepository.findOne({
      where: { id },
    });
  }

  async insertCar(insertCarDto: InsertCarDto): Promise<Car> {
    const insertedCar = this.create(insertCarDto);
    const { carPhotoIds } = insertCarDto;
    if (carPhotoIds && carPhotoIds.length > 0) {
      const photos = await this.carPhotoRepository.findByIds([...carPhotoIds]);
      insertedCar.carPhotos = [...photos];
    }
    return await this.carRepository.save(insertedCar);
  }

  async uploadPhotos(files: Express.Multer.File[]): Promise<CarPhoto[]> {
    const carPhotos = [];
    const cloudPromises: Array<Promise<UploadedData>> = [];
    files.forEach(file => {
      cloudPromises.push(
        this.cloudStorageService.uploadFile('korakotmotors.com', file),
      );
    });
    const uploadedFiles = await Promise.all(cloudPromises);
    uploadedFiles.forEach(file => {
      carPhotos.push(
        this.carPhotoRepository.create({
          photoName: file.originalname,
          photoPath: file.path,
          photoUrl: file.publicUrl,
        }),
      );
    });
    const uploadedPhotos = await this.carPhotoRepository.save(carPhotos);
    return uploadedPhotos;
  }

  async updateCar(carId: number, updateCarDto: UpdateCarDto): Promise<Car> {
    const foundCar = await this.findById(carId);
    const updatedCar = { ...foundCar, ...updateCarDto };
    return await this.carRepository.save(updatedCar);
  }

  async deleteCar(carId: number): Promise<string> {
    const car = await this.findById(carId);
    await this.carRepository.remove(car);
    return `Car id: ${carId} has been removed`;
  }
}
