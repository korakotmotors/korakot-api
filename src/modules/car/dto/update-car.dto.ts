import { Brand, TransmissionType, BodyType } from '../../../common/constants';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateCarDto {
  @ApiModelPropertyOptional()
  registrationNumber?: string;

  @ApiModelPropertyOptional({ enum: Brand })
  brand?: Brand;

  @ApiModelPropertyOptional()
  model?: string;

  @ApiModelPropertyOptional({ enum: BodyType })
  bodyType?: BodyType;

  @ApiModelPropertyOptional()
  variant?: string;

  @ApiModelPropertyOptional()
  registeredYear?: number;

  @ApiModelPropertyOptional()
  price?: number;

  @ApiModelPropertyOptional()
  sellPrice?: number;

  @ApiModelPropertyOptional()
  interest?: number;

  @ApiModelPropertyOptional()
  mileage?: number;

  @ApiModelPropertyOptional({ enum: TransmissionType })
  transmission?: TransmissionType;

  @ApiModelPropertyOptional()
  color?: string;

  @ApiModelPropertyOptional()
  seatCapacity?: number;

  @ApiModelPropertyOptional()
  engineCapacity?: number;
}
