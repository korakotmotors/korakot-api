import { Brand, TransmissionType, BodyType } from '../../../common/constants';
import { ApiModelProperty } from '@nestjs/swagger';

export class InsertCarDto {
  @ApiModelProperty()
  registrationNumber: string;

  @ApiModelProperty({ enum: Brand })
  brand: Brand;

  @ApiModelProperty()
  model: string;

  @ApiModelProperty({ enum: BodyType })
  bodyType: BodyType;

  @ApiModelProperty()
  variant: string;

  @ApiModelProperty()
  registeredYear: number;

  @ApiModelProperty()
  price: number;

  @ApiModelProperty()
  sellPrice: number;

  @ApiModelProperty()
  interest: number;

  @ApiModelProperty()
  mileage: number;

  @ApiModelProperty({ enum: TransmissionType })
  transmission: TransmissionType;

  @ApiModelProperty()
  color: string;

  @ApiModelProperty()
  seatCapacity: number;

  @ApiModelProperty()
  engineCapacity: number;

  @ApiModelProperty()
  carPhotoIds?: number[];
}
