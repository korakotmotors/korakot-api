import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  UseInterceptors,
  UploadedFiles,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { CarService } from './car.service';
import { Car } from '../../common/entities/car.entity';
import { InsertCarDto } from './dto/insert-car.dto';
import { ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { UpdateCarDto } from './dto/update-car.dto';
import { FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path = require('path');

const dir = `${__dirname}/../../../uploads/images`;
@Controller()
export class CarController {
  constructor(private readonly carService: CarService) {}

  @Get('cars')
  findAll(): Promise<Car[]> {
    return this.carService.findAll();
  }

  @Get('car/:id')
  findById(@Param('id') id: number): Promise<Car> {
    return this.carService.findById(id);
  }

  @Post('car')
  insertCar(@Body() insertCarDto: InsertCarDto): Promise<Car> {
    return this.carService.insertCar(insertCarDto);
  }

  @Post('upload')
  @UseInterceptors(
    FilesInterceptor('files', 6, {
      storage: diskStorage({
        destination: dir,
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
      limits: {
        fileSize: 2097152,
      },
      fileFilter: (req, file, callback) => {
        const ext = path.extname(file.originalname);
        if (file.mimetype === 'image/jpeg') {
          return callback(null, true);
        }
        return callback(
          new HttpException(
            'image type JPEG or JPG only',
            HttpStatus.BAD_REQUEST,
          ),
          false,
        );
      },
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({
    name: 'files',
    required: true,
    description: 'Upload car photos',
  })
  async uploadPhotos(@UploadedFiles() files: Express.Multer.File[]) {
    return await this.carService.uploadPhotos(files);
  }

  @Put('car/:id')
  updateCar(
    @Param('id') id: number,
    @Body() updateCarDto: UpdateCarDto,
  ): Promise<Car> {
    return this.carService.updateCar(id, updateCarDto);
  }

  @Delete('car/:id')
  deleteCar(@Param('id') id: number): Promise<string> {
    return this.carService.deleteCar(id);
  }
}
