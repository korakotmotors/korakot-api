import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { ObjectSchema, object, string, number, boolean, validate } from 'joi';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: ObjectSchema = object({
      NODE_ENV: string()
        .valid(['development', 'production'])
        .default('development'),
      DB_HOST: string(),
      DB_PORT: number().default(3306),
      DB_USERNAME: string(),
      DB_PASSWORD: string(),
      DB_NAME: string(),
      DB_LOGGING: string(),
      DB_SYNC: string(),
      GCS_PROJECT_ID: string(),
      GCS_KEY_FILENAME: string(),
      JWT_SECRET: string(),
      REDIS_PASSWORD: string(),
      REDIS_HOST: string(),
      REDIS_PORT: number().default(6379),
      REDIS_DB: number().default(0),
    });

    const { error, value: validatedEnvConfig } = validate(
      envConfig,
      envVarsSchema,
    );

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedEnvConfig;
  }

  getString(key: string): string {
    return this.envConfig[key];
  }

  getRedisConnectionString(): string {
    console.log(
      `redis://:${this.getString('REDIS_PASSWORD')}@${this.getString(
        'REDIS_HOST',
      )}:${this.getString('REDIS_PORT')}/${this.getString('REDIS_DB')}`,
    );
    return `redis://:${this.getString('REDIS_PASSWORD')}@${this.getString(
      'REDIS_HOST',
    )}:${this.getString('REDIS_PORT')}/${this.getString('REDIS_DB')}`;
  }
}
