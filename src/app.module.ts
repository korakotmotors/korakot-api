import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { CarModule } from './modules/car/car.module';
import { MulterModule } from '@nestjs/platform-express';
import { join } from 'path';
import { ConfigModule } from './modules/config/config.module';
import { CloudStorageModule } from './modules/google-cloud-storage/cloud-storage.module';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { ConfigService } from './modules/config/config.service';
import { RedisService } from './modules/redis.service';

const dir = join(__dirname, '/../uploads/images');

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        const typeOrmConfig: TypeOrmModuleOptions = {
          type: 'mariadb',
          host: configService.getString('DB_HOST'),
          port: Number(configService.getString('DB_PORT')),
          username: configService.getString('DB_USERNAME'),
          password: configService.getString('DB_PASSWORD'),
          database: configService.getString('DB_NAME'),
          entities: [__dirname + '/**/**.entity{.ts,.js}'],
          synchronize: true,
          logging: true,
        };
        return typeOrmConfig;
      },
      inject: [ConfigService],
    }),
    MulterModule.register(),
    CarModule,
    CloudStorageModule,
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
