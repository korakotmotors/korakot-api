FROM node:10-alpine as baseImage
WORKDIR /app
ENV TZ=Asia/Bangkok
RUN apk add --no-cache tzdata

FROM baseImage as builder
RUN apk add -U build-base python
RUN mkdir -p /src/build
COPY yarn.lock package*.json /src/
RUN cd /src && yarn --prod && cp -r node_modules /src/build/node_modules
COPY . /app
RUN yarn prestart:prod
RUN cp -a /app/dist/. /src/build/

FROM baseImage
ENV HOME /app
COPY --from=builder /src/build/ .
COPY .environment .
RUN cd /app
RUN cat ENV_PROD >production.env
CMD NODE_ENV=production node main.js
